from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from .models import Persona,Perro,Registro
from django.shortcuts import redirect

from django.contrib.auth.models import User

from django.contrib.auth import authenticate,logout, login as auth_login

from django.contrib.auth.decorators import login_required,permission_required

from django.contrib.auth.models import Permission

from registro.models import Persona

import datetime

from django.contrib import messages

from django.core.exceptions import ObjectDoesNotExist
 




# Create your views here.

def index(request):
    usuario = request.session.get('usuario',None)
    return render(request,'index.html', {'perritos': Perro.objects.all(), 'usuario':usuario})

def formulario(request):
    return render(request,'formulario.html', {})

def login(request):
    return render(request,'login.html', {})

def politicasdeprivacidad(request):
    return render(request,'politicasdeprivacidad.html', {})

def crearPersona(request):


    email= request.POST.get('email','')
    run = request.POST.get('run','')
    contrasenia = request.POST.get('contrasenia','')
    nombre = request.POST.get('nombre','')
    fechanac = request.POST.get('fechanac','')
    telefono = request.POST.get('telefono',0)
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    tipovivienda = request.POST.get('vivienda','')

    persona = Persona(email=email,run=run,contrasenia=contrasenia,nombre=nombre,fechanac=fechanac,telefono=telefono
    ,region=region,comuna=comuna,tipovivienda=tipovivienda)

    
    try:
        pe = Persona.objects.get(email=persona.email)
        if pe is None:
             
            
            permission = Permission.objects.get(name='is adoptante')
            p = User.objects.create_user(username=persona.email,password=persona.contrasenia,first_name=persona.nombre,email=email)
            p.save()
            #p = User.objects.get(username=persona.email)
            p.user_permissions.add(permission)
            persona.save()
        else:

            messages.error(request,'Este correo ya esta registrado, intente con otro.')
            return redirect('formulario')
    except ObjectDoesNotExist:
            
            permission = Permission.objects.get(name='is adoptante')
            p = User.objects.create_user(username=persona.email,password=persona.contrasenia,first_name=persona.nombre,email=email)
            p.save()
            #p = User.objects.get(username=persona.email)
            p.user_permissions.add(permission)
            persona.save()
            return redirect('login')


@login_required(login_url='login')
def crearPerro(request):
    nombre = request.POST.get('nombre','')
    raza = request.POST.get('raza','')
    foto = request.FILES.get('foto',False)
    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado','')
    perro = Perro(nombre=nombre,raza=raza,foto=foto,descripcion=descripcion,estado=estado)
    perro.save()
    return redirect('mantenedor')

@login_required(login_url='login')
def eliminarPerro(request,id):
    perro = Perro.objects.get(pk = id)
    perro.delete()

    

    return redirect('mantenedor')

# @login_required(login_url='login')
# def editar(request,id):
#     perro = Perro.objects.get(pk=id)
#     return render(request,'index.html', {'persona':perro})

@login_required(login_url='login')
def cargaEditar(request,id):
    p = Perro.objects.get(pk=id)
    usuario = request.session.get('usuario',None)
    return render(request,'editarPerro.html',{'perro':p},{'usuario':usuario})

@login_required(login_url='login')
def editarPerro(request,id):
    perro = Perro.objects.get(pk = id)
    nombre = request.POST.get('nombre','')
    raza = request.POST.get('raza','')
    foto = request.FILES.get('foto',perro.foto)

    descripcion = request.POST.get('descripcion','')
    estado = request.POST.get('estado',perro.estado)
    perro.nombre = nombre
    perro.raza = raza
    perro.foto = foto
    perro.descripcion = descripcion
    perro.estado = estado
    perro.save()
    return redirect('mantenedor')

@login_required(login_url='login')
def cerrar_session(request):
    del request.user
    logout(request)
    return redirect('index')

@login_required(login_url='login')
def mantenedor(request):
    usuario = request.session.get('usuario',None)
    user = request.user
    if user.has_perm('registro.is_administrador'):

        return render(request,'mantenedor.html',{'name':'Registro de perros','perros':Perro.objects.all(),'usuario':usuario})

    elif user.has_perm('registro.is_adoptante'):

        auth_login(request, user)
        request.session['usuario'] = user.first_name
        return render(request,'adopcion.html', {'perros': Perro.objects.all(),'usuario':usuario})


    else:
        return render(request,'login.html')



def login_iniciar(request):
    username = request.POST.get('nombre','')
    contrasenia = request.POST.get('contrasenia','')
    user = authenticate(request,username=username,password=contrasenia)

    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect("mantenedor")
    else:
        return redirect("login")

@login_required(login_url='login')
def adopcion(request):
    usuario = request.session.get('usuario',None)
    return render(request,'adopcion.html', {'perros': Perro.objects.all(),'usuario':usuario})

@login_required(login_url='login')
def adoptar(request,id,usuario):
    u = request.session.get('usuario',None)
    #p = Persona.objects.get(username = )
    perro = Perro.objects.get(pk = id)
    estado = 'Adoptado'
    perro.estado = estado
    perro.save()

    adoptante = usuario
    fecha = datetime.datetime.now()
    nombreperro = perro.nombre
    foto = perro.foto

    registro = Registro(adoptante=adoptante,fecha=fecha,nombreperro=nombreperro,foto=foto)
    registro.save()


    return render(request,'adopcion.html', {'perros': Perro.objects.all(),'usuario':u})
@login_required(login_url='login')
def mostrarRegistro(request):
    return render(request,'mostrarRegistro.html', {'registro':Registro.objects.all()})


