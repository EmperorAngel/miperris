# Generated by Django 2.1.2 on 2018-11-17 22:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0009_auto_20181104_1926'),
    ]

    operations = [
        migrations.AlterField(
            model_name='persona',
            name='fechanac',
            field=models.DateTimeField(null=True),
        ),
    ]
