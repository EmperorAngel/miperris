# Generated by Django 2.1.3 on 2018-11-04 21:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0005_auto_20181104_0133'),
    ]

    operations = [
        migrations.CreateModel(
            name='Registro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('adoptante', models.CharField(max_length=40)),
                ('fecha', models.DateTimeField()),
                ('nombreperro', models.CharField(max_length=40)),
            ],
        ),
    ]
