from django.db import models
from .validators import validate_file_extension
from django.utils.translation import ugettext as _
from django.dispatch import receiver
from allauth.account.signals import user_signed_up
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission

from django.db.models.signals import pre_delete, post_delete

from django.conf import settings
import os


# Create your models here.



class Persona(models.Model):
    email= models.CharField(max_length=100)
    run = models.CharField(max_length=25)
    contrasenia = models.CharField(max_length=50)
    nombre = models.CharField(max_length=100)
    fechanac = models.DateTimeField(null=True)
    telefono = models.CharField(max_length=50)
    region = models.CharField(max_length=50)
    comuna = models.CharField(max_length=50)
    tipovivienda = models.CharField(max_length=50)

    def __str__(self):
        return "nombre: "+self.nombre

    class Meta:

        permissions = (
            ('is_administrador',_('is administrador')),
            ('is_adoptante',_('is adoptante'))
        )

class Perro(models.Model):
    nombre = models.CharField(max_length=40)
    raza = models.CharField(max_length=40)
    foto = models.ImageField(upload_to="fotos/",validators=[validate_file_extension])
    descripcion = models.CharField(max_length=40)
    estado = models.CharField(max_length=40)
    

    def __str__(self):
        return "PERRO"


class Registro(models.Model):
    adoptante = models.CharField(max_length=40)
    fecha = models.DateTimeField()
    nombreperro = models.CharField(max_length=40)
    foto = models.ImageField(upload_to="fotos/",validators=[validate_file_extension])
    

    def __str__(self):
        return "REGISTRO"


@receiver(user_signed_up)
def create_user_profile(request, user, **kwargs):
    user.username= user.username+"_FB"
    user.save()
    persona = Persona(email=user.email,nombre=user.first_name+" "+user.last_name)
    persona.save()
    p = User.objects.get(email=persona.email)
    permission = Permission.objects.get(name='is adoptante')
    p.user_permissions.add(permission)
    p.save()

@receiver(pre_delete, sender=Perro)
def _Perro_delete(sender, instance, using, **kwargs):
    file_path = settings.BASE_DIR + str(instance.foto)
    print(file_path)

    if os.path.isfile(file_path):
        os.remove(file_path)