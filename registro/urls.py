from django.conf import settings
from django.conf.urls.static import static

from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from django.urls import path, include
from registro import views as user_views



urlpatterns = [
    path('',views.index,name='index'),
    path('adopcion/',views.adopcion,name='adopcion'),
    path('adopcion/<int:id>/<str:usuario>',views.adoptar,name='adoptar'),
    path('mostrarRegistro/',views.mostrarRegistro,name='mostrarRegistro'),
    path('formulario/',views.formulario,name='formulario'),
    path('formulario/crear',views.crearPersona,name='crear'),
    path('login/',views.login,name='login'),
    path('mantenedor/',views.mantenedor,name="mantenedor"),
    path('login/iniciar',views.login_iniciar,name="iniciar"),
    path('cerrarsession/',views.cerrar_session,name="cerrar_session"),
    path('registro/crearPerro',views.crearPerro,name="crearPerro"),
    path('eliminarPerro/<int:id>',views.eliminarPerro,name="eliminarPerro"),
    path('editarPerro/edit/<int:id>',views.editarPerro,name="editarPerro"),
    path('editarPerro/<int:id>',views.cargaEditar,name="cargaEditar"),
    path('password-reset/',
         auth_views.PasswordResetView.as_view(
             template_name='register/password_reset.html'
         ),
         name='password_reset'),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(
             template_name='register/password_reset_done.html'
         ),
         name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(
             template_name='register/password_reset_confirm.html'
         ),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='register/password_reset_complete.html'
         ),
         name='password_reset_complete'),
    path('', include('blog.urls')),
    path('accounts/', include('allauth.urls')),
    path('politicasdeprivacidad/',views.politicasdeprivacidad,name='politicasdeprivacidad'),

    #path('cambiarpass/',views.cambiarpass,name="cambiarpass"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    
